<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

# Videos Pepito Front-End

El FrontEnd de videos pepito fue desarrollado en el sistema de plantillas de Laravel (Blade) apoyándose de Bootstrap 4, Jquery y CryptoJS (este ultimo usado para cifrar los datos de los usuarios fuente https://code.google.com/archive/p/crypto-js/ ), el consumo de la API videospepitoback es realizado con AJAX.

## Empezando

Estas instrucciones le brindarán una copia del proyecto en funcionamiento en su máquina local para fines de desarrollo y prueba.

### Prerequisites

leer Requisitos del servidor para Laravel 5.8
los más importantes son: 

Contar con:
```
PHP> = 7.1.3
Composer

```

### Installación

Luego de tener clonado el repositorio, es necesario ejecutar los siguientes pasasos:

##### Actualizar composer 

```
composer update
```

##### Generar el archivo de configuracion .env

```
copy .env.example .env
```

##### Generar la key de laravel 

```
php artisan key:generate
```


NOTA: configurar VirtualHost para que el acceso a este proyecto sea http://videospepito.loc

Si por alguna razón el VirtualHost no se puede configurar será necesario actualizar dos variables que se encuentran en el .env

```
APP_URL_SERVER=http://videospepitoback.loc/
APP_URL_SERVER_API=http://videospepitoback.loc/api/
```

Y cambiar los valores que correspondan a las ruta de la API 

La API ya cuenta con unas semillas iniciales de datos pero si se necesita hacer pruebas para agregar nuevas películas pueden sacarlas de aquí 

https://listas.20minutos.es/lista/250-peliculas-que-tienes-que-ver-antes-de-morir-345858/


