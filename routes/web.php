<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('home');})->name('home');

Route::get('/catalogo', function () {return view('catalogo');})->name('catalogo');

Route::get('/misreservas', function () {return view('misreservas');})->name('misreservas');

Route::get('/alquilar', function () {return view('alquilar');})->name('alquilar');

Route::get('/pelicula', function () {return view('pelicula');})->name('pelicula');

Route::get('/usuarios', function () {return view('usuarios');})->name('usuarios');