@extends('layouts.layout   ')

@section('styles')



@stop

@section('content')

    <div class="page-holder">
        <!-- Hero Section-->
        <section class="shape-1 shape-1-sm">
            <div class="container">
                <div class="row">
                    <div id="divSubirPeli" class="col-lg-1 mt-5 consesion" style="display: none">
                        <button type="button" data-toggle="modal" data-target="#modalSubirPeli" class="btn btn-primary">Subir nueva Pelicula</button>
                    </div>
                    <div class="col-lg-7 mx-auto">

                        <div class="subscription-form mt-5">
                            <div class="form-group">
                                <label>Buscar Pelicula</label>
                                <input type="text" id="nombre" placeholder="La vida es bella" class="form-control">
                                <button type="button" onclick="paginacion()" class="btn btn-primary">Buscar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Schedule Section-->
        <section class="schedule shape-2">
            <div class="container">
                <div class="schedule-table">
                    <nav>
                        <div id="nav-tab" role="tablist" class="nav nav-tabs schedule-nav nav-fill">

                            <a id="nav-mon-tab" data-toggle="tab" href="#" onclick="cambiarGenero('')" role="tab" aria-selected="true"
                               class="nav-item nav-link schedule-nav-link ">Todos los Generos</a>
                            <a id="nav-mon-tab" data-toggle="tab" href="#" onclick="cambiarGenero('comedia')" role="tab" aria-selected="true"
                               class="nav-item nav-link schedule-nav-link ">Comedia</a>
                            <a id="nav-mon-tab" data-toggle="tab" href="#" onclick="cambiarGenero('accion')" role="tab" aria-selected="true"
                               class="nav-item nav-link schedule-nav-link ">Acción</a>
                            <a id="nav-tue-tab" data-toggle="tab" href="#" onclick="cambiarGenero('terror')" role="tab" aria-selected="false"
                               class="nav-item nav-link schedule-nav-link">Terror</a>
                            <a id="nav-wed-tab" data-toggle="tab" href="#" onclick="cambiarGenero('infantil')" role="tab" aria-selected="false"
                               class="nav-item nav-link schedule-nav-link">Infantil</a>
                            <a id="nav-thu-tab" data-toggle="tab" href="#" onclick="cambiarGenero('suspenso')" role="tab" aria-selected="false"
                               class="nav-item nav-link schedule-nav-link">Suspenso</a>
                        </div>
                    </nav>
                    <div id="nav-tabContent" class="tab-content">


                    </div>
                    <div id="pagination" class="mt-5">

                    </div>

                </div>
            </div>
        </section>
    </div>


    <!-- Subir Nueva pelicula  Modal-->
    <div id="modalSubirPeli" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade bd-example-modal-lg">
        <div role="document" class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header border-bottom-0">
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>

                <div class="modal-body p-4 p-lg-5">
                    <h5>Subir una Pelicula</h5>
                    <form action="#" class="login-form text-left" id="formSubirPelicula">
                        <div class="form-group mb-4">
                            <label>Portada</label>
                            <input type="text" name="portada" placeholder="Imagen de portada de la pelicula (base64 -> data:image/jpeg;base64,/9j/4AAQS...)" class="form-control" required>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Nombre</label>
                                <input type="text" name="nombre" placeholder="Nombre de la Pelicula" class="form-control" required>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Genero</label>
                                <select class="form-control" name="genero" required>
                                    <option>Comedia</option>
                                    <option>Acción</option>
                                    <option>Terror</option>
                                    <option>Infantil</option>
                                    <option>Suspenso</option>
                                </select>
                            </div>
                            <div class="form-group mt-5 col-md-4">
                                <input type="checkbox" id="cartelera" class="filled-in">
                                <label for="remember_me">En Cartelera</label>
                            </div>
                        </div>

                        <div class="form-group mb-4">
                            <label>Descripción</label>
                            <input type="text" name="descripcion" placeholder="Una descriopción sobre lo que trata la pelicula" class="form-control" required>
                        </div>

                        <div class="row">
                            <div class="form-group  col-md-4">
                                <label>Año de lanzamiento</label>
                                <input type="text" name="fecha_lanzamiento" placeholder="Año de lanzamiento de la pelicula" class="form-control" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Costo</label>
                                <input type="text" name="costo" placeholder="Costo del alquiler de la pelicula" class="form-control" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Cantidad</label>
                                <input type="text" name="cantidad" placeholder="Unidades exitentes de la pelicula" class="form-control" required>
                            </div>
                        </div>

                        <div id="alertErrorLogin" class="alert alert-danger alert-dismissible" style="display: none">
                            <strong>Error!</strong> <span id="errorLogin"></span>
                        </div>

                        <div class="form-group">
                            <input type="submit" value="Iniciar Sesión" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@stop

@section('scripts')

    <script>

        var genero = "";
        var pagina = 1;


        $(function () {
            paginacion(pagina);

            if (!localStorage.getItem("rol")) {
                $("#divSubirPeli").remove();
            }else{
                $("#divSubirPeli").show();
            }



            var formSubirPelicula = $("#formSubirPelicula");

            formSubirPelicula.submit(function (e) {
                e.preventDefault();
                $.ajax({
                    url: "{{env("APP_URL_SERVER_API")}}pelicula/store",
                    data: formSubirPelicula.serialize()+"&token="+ localStorage.getItem('access_token'),
                    type: 'POST',
                    dataType: 'json',

                    success: function (result) {

                        $("#modalSubirPeli").modal("hide");
                        $("#formSubirPelicula")[0].reset();
                        paginacion(pagina);

                    },
                    error: function (xhr, status) {
                        if (xhr.status == 401) {

                            $("#errorLogin").html(xhr.responseJSON.error);
                            $("#alertErrorLogin").show();

                        } else {
                            var message = "Error de ejecución: " + xhr.status + " " + xhr.statusText;
                            swal(
                                'Error!!',
                                message,
                                'error'
                            )
                        }

                    },
                    // código a ejecutar sin importar si la petición falló o no
                    complete: function (xhr, status) {
                        // fincarga();
                    }
                });
            });


        });


        function paginacion(page = pagina) {
            var nombre = $("#nombre").val();
            $.ajax({
                url: "{{env("APP_URL_SERVER_API")}}pelicula/peliculasfiltro",
                data: {"nombre": nombre, "genero": genero, "perPage": 3, "page": page},
                type: 'GET',
                dataType: 'json',
                beforeSend: function () {
                    //cargando();
                },
                success: function (result) {

                    $("#nav-tabContent").html("");

                    var coloresGenero = {"comedia": 1, "accion": 2, "terror": 3, "infantil": 4, "suspenso": 5};

                    $.each(result.data.data, function (i, d) {

                        var html = "<div class='schedule-table-item'>" +
                            "<div class='row align-items-center'>" +
                            "<div class='col-lg-3'>" +
                            "<a href='#' onClick='pelicula(" + d.id + ")' class='schedule-item-image'><img src='{{env("APP_URL_SERVER")}}imagenes/" + d.portada + "' alt='...' class='img-fluid'></a>" +
                            "</div> " +
                            "<div class='col-lg-9'><span class='schedule-item-genre gradient-" + coloresGenero[d.genero] + "'>" + d.genero + "</span> " +
                            "<h3 class='schedule-item-name'>" + d.nombre + " ( " + d.fecha_lanzamiento + " )</h3> " +
                            "<p class='schedule-item-description'>" + d.descripcion + "</p> " +

                            "<div class='schedule-item-time'>" +
                            "<div class='viewing-times'> " +
                            "<span><i class='icon ion-md-cash'></i> $ " + number_format(d.costo, 0) + "</span>" +
                            "</div>" +
                            "</div>";


                        if (localStorage.getItem("username")) {
                            html += "<div class='col consesion'>" +
                                "<button class='btn btn-primary' onclick='reservar(" + d.id + ")'> RESERVAR</button>" +
                                "</div>";
                        }


                        html += "</div> " +
                            "</div> " +
                            "</div> ";

                        $("#nav-tabContent").append(html);

                    });


                    pagina = result.data.current_page;
                    var htmlPagination = "<ul class='pagination justify-content-center'>" +
                        "<li class='page-item " + ((result.data.prev_page_url == null) ? "disabled" : "") + "'> " +
                        "<a href='#' onclick='paginacion(" + (pagina - 1) + ")' class='page-link'>Anterior</a> " +
                        "</li> ";

                    for (var i = 1; i <= result.data.last_page; i++) {

                        htmlPagination += "<li class='page-item " + ((i == pagina) ? "active" : "") + "'> " +
                            "<a href='#' onclick='paginacion(" + i + ")' class='page-link'>" + i + "</a> " +
                            "</li> ";

                    }

                    htmlPagination += "<li class='page-item " + ((result.data.next_page_url == null) ? "disabled" : "") + "'> " +
                        "<a href='#' onclick='paginacion(" + (pagina + 1) + ")' class='page-link'>Siguiente</a> " +
                        "</li> " +
                        "</ul>";

                    $("#pagination").html(htmlPagination);

                },
                error: function (xhr, status) {
                    if (xhr.status == 401) {

                        $("#errorLogin").html(xhr.responseJSON.error);
                        $("#alertErrorLogin").show();

                    } else {
                        var message = "Error de ejecución: " + xhr.status + " " + xhr.statusText;
                        swal(
                            'Error!!',
                            message,
                            'error'
                        )
                    }

                },
                // código a ejecutar sin importar si la petición falló o no
                complete: function (xhr, status) {
                    // fincarga();
                }
            });
        }

        function cambiarGenero(valor) {
            genero = valor;
            paginacion(pagina);

        }

    </script>


@stop
