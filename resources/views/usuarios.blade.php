@extends('layouts.layout   ')

@section('styles')

    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">

@stop

@section('content')

    <section class="">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12 mt-5">
                    <h4>Lista de Usuaurios</h4>
                    <br/>
                    <div class="table-responsive">
                        <table class="table table-striped" id="users-table" width="100%">
                            <thead>
                            <tr>
                                <th>Documento</th>
                                <th>Nombre</th>
                                <th>NickName</th>
                                <th>telefono</th>
                                <th>Estado</th>
                                <th>Accion(s)</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </section>


@stop

@section('scripts')
    <script src="{{asset('plugins/jquery/jquery.datatables.min.js')}}"></script>
    <script>

        var table;

        $(function () {

            if (!localStorage.getItem("rol")) {
                window.location = "/";
            }

            $.fn.dataTable.render.ninkname = function () {
                return function (data, type, row) {

                    console.log(AESdencrypt(decodeEntities(data), localStorage.getItem("access_token")));
                    return data;
                };
            };

            table = $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                "language": {
                    "url": "js/Spanish.json"
                },
                ajax: {
                    url: "{{env("APP_URL_SERVER_API")}}usersall?token=" + localStorage.getItem('access_token'),
                    "type": "get",
                },
                columns: [
                    {
                        data: 'documento', name: 'documento',
                        render: function (data, type, row) {
                            return AESdencrypt(decodeEntities(data), localStorage.getItem("access_token"));
                        }
                    },
                    {
                        data: 'nombre', name: 'nombre',
                        render: function (data, type, row) {
                            return AESdencrypt(decodeEntities(data), localStorage.getItem("access_token"));
                        }
                    },
                    {
                        data: 'nickname', name: 'nickname',
                        render: function (data, type, row) {
                            return AESdencrypt(decodeEntities(data), localStorage.getItem("access_token"));
                        }
                    },
                    {
                        data: 'telefono', name: 'telefono',
                        render: function (data, type, row) {
                            return AESdencrypt(decodeEntities(data), localStorage.getItem("access_token"));
                        }
                    },
                    {data: 'estado', name: 'estado'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });

        });


        function cambiarestado(user_id) {
            $.ajax({
                url: "{{env("APP_URL_SERVER_API")}}user/cambiarestado",
                data: {"token": localStorage.getItem('access_token'), "user_id": user_id},
                type: 'POST',
                dataType: 'json',

                success: function (result) {
                    table.ajax.reload();
                    jQuery.gritter.add({
                        title: '¡Bien!',
                        text: result.message,
                        class_name: 'growl-success',
                        image: '{{asset("img/plugins/screen.png")}}',
                        sticky: false,
                        time: ''
                    });

                },
                error: function (xhr, status) {
                    if (xhr.status == 401) {

                        $("#errorLogin").html(xhr.responseJSON.error);
                        $("#alertErrorLogin").show();

                    } else {
                        var message = "Error de ejecución: " + xhr.status + " " + xhr.statusText;
                        swal(
                            'Error!!',
                            xhr.responseJSON.message,
                            'error'
                        )
                    }

                },
                // código a ejecutar sin importar si la petición falló o no
                complete: function (xhr, status) {
                    // fincarga();
                }
            });
        }


    </script>


@stop
