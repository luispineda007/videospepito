@extends('layouts.layout   ')

@section('styles')



@stop

@section('content')

    <div class="page-holder">
        <!-- Hero Section-->
        <section class="shape-1 shape-1-sm">
            <div class="container">

            </div>
        </section>
        <!-- Schedule Section-->
        <section class="schedule shape-2">
            <div class="container">
                <div class="schedule-table">
                    <div id="nav-tabContent" class="tab-content">

                    </div>
                    <div id="pagination" class="mt-5">

                    </div>

                    <div class='row justify-content-center' ></div>
                    <div class='col-3'></div>

                </div>
            </div>
        </section>
    </div>


@stop

@section('scripts')

    <script>

        var pagina = 1;

        $(function () {

            //Validamos si existe un usuario de lo contrario lo redireccionamos a una vista que no necesrite estar loggeado
            if(!localStorage.getItem("username")){
                window.location="/";
            }

            //Validamos que el rol sea Administrador para poder estar en esta area
            if(!localStorage.getItem("rol")){
                window.location="/";
            }



            paginacion(pagina);

        });

        function paginacion(page = pagina) {

            $.ajax({
                url: "{{env("APP_URL_SERVER_API")}}reservas",
                data: {"token":localStorage.getItem('access_token')},
                type: 'GET',
                dataType: 'json',
                beforeSend: function () {
                    //cargando();
                },
                success: function (result) {

                    $("#nav-tabContent").html("");

                    var coloresGenero = {"comedia": 1, "accion": 2, "terror": 3, "infantil": 4, "suspenso": 5};

                    $.each(result.data, function (i, d) {

                        var html = "<div id='"+d.id+"' class='schedule-table-item'>" +
                            "<div class='row align-items-center'>" +
                            "<div class='col-lg-2 text-center'>" +
                            "<a href='#' onClick='pelicula("+d.id+")' class='schedule-item-image'><img width='60px' src='{{env("APP_URL_SERVER")}}imagenes/" + d.pelicula.portada + "' alt='...' class='img-fluid'></a>" +
                            "</div> " +
                            "<div class='col-lg-10'> <div class='row'>" +
                            "<div class='col-lg-6'>" +
                            "<h5 class='schedule-item-name'>   " +
                            "<span class='schedule-item-genre gradient-" + coloresGenero[d.pelicula.genero] + "'> " + d.pelicula.genero + "</span>  " + d.pelicula.nombre + " ( " + d.pelicula.fecha_lanzamiento + " )" +
                            "</h5> " +
                            " <b>Usuario: </b>" + d.user.nombre + "<br> <b> documento:  </b>" +  d.user.documento +
                            "</div> " +

                            "<div class='col-lg-2 schedule-item-time'>" +
                            "<div class='viewing-times'> " +
                            "<span><i class='icon ion-md-cash'></i> $ "+number_format(d.pelicula.costo,0)+"</span>" +
                            "</div>" +
                            "</div> " +
                            "<div class='col-lg-4 text-center'>" +
                            "<button class='btn btn-primary btn-sm' onclick='alquilar("+d.id+")'> ALQUILAR</button>" +
                            "<button class='btn btn-danger btn-sm' onclick='cancelarReservar("+d.id+")'> CANCELAR</button>" +

                            "</div>" +
                            "</div>" +
                            "</div> " +
                            "</div> " +
                            "</div> ";

                        $("#nav-tabContent").append(html);

                    });


                    pagina = result.data.current_page;
                    var htmlPagination = "<ul class='pagination justify-content-center'>" +
                        "<li class='page-item " + ((result.data.prev_page_url == null) ? "disabled" : "") + "'> " +
                        "<a href='#' onclick='paginacion(" + (pagina - 1) + ")' class='page-link'>Anterior</a> " +
                        "</li> ";

                    for (var i = 1; i <= result.data.last_page; i++) {

                        htmlPagination += "<li class='page-item " + ((i == pagina) ? "active" : "") + "'> " +
                            "<a href='#' onclick='paginacion(" + i + ")' class='page-link'>" + i + "</a> " +
                            "</li> ";

                    }

                    htmlPagination += "<li class='page-item " + ((result.data.next_page_url == null) ? "disabled" : "") + "'> " +
                        "<a href='#' onclick='paginacion(" + (pagina + 1) + ")' class='page-link'>Siguiente</a> " +
                        "</li> " +
                        "</ul>";

                    $("#pagination").html(htmlPagination);

                },
                error: function (xhr, status) {
                    if (xhr.status == 401) {

                        $("#errorLogin").html(xhr.responseJSON.error);
                        $("#alertErrorLogin").show();

                    } else {
                        var message = "Error de ejecución: " + xhr.status + " " + xhr.statusText;
                        swal(
                            'Error!!',
                            message,
                            'error'
                        )
                    }

                },
                // código a ejecutar sin importar si la petición falló o no
                complete: function (xhr, status) {
                    // fincarga();
                }
            });
        }

        function cambiarGenero(valor) {
            genero = valor;
            paginacion(pagina);

        }


        function alquilar(id) {

            $.ajax({
                url: "{{env("APP_URL_SERVER_API")}}alquilar",
                data: {"token": localStorage.getItem('access_token'),"reserva_id":id},
                type: 'POST',
                dataType: 'json',

                success: function (result) {
                    jQuery.gritter.add({
                        title: '¡Bien!',
                        text: result.message,
                        class_name: 'growl-success',
                        image: '{{asset("img/plugins/screen.png")}}',
                        sticky: false,
                        time: ''
                    });
                    $("#"+id).remove();

                },
                error: function (xhr, status) {
                    if (xhr.status == 401) {

                        $("#errorLogin").html(xhr.responseJSON.error);
                        $("#alertErrorLogin").show();

                    } else {
                        var message = "Error de ejecución: " + xhr.status + " " + xhr.statusText;
                        swal(
                            'Error!!',
                            message,
                            'error'
                        )
                    }

                },
                // código a ejecutar sin importar si la petición falló o no
                complete: function (xhr, status) {
                    // fincarga();
                }
            });
        }

        function cancelarReservar(id) {

            $.ajax({
                url: "{{env("APP_URL_SERVER_API")}}reserva/cancelar",
                data: {"token": localStorage.getItem('access_token'),"reserva_id":id},
                type: 'POST',
                dataType: 'json',

                success: function (result) {
                    jQuery.gritter.add({
                        title: '¡Bien!',
                        text: result.message,
                        class_name: 'growl-success',
                        image: '{{asset("img/plugins/screen.png")}}',
                        sticky: false,
                        time: ''
                    });
                    $("#"+id).remove();

                },
                error: function (xhr, status) {
                    if (xhr.status == 401) {

                        $("#errorLogin").html(xhr.responseJSON.error);
                        $("#alertErrorLogin").show();

                    } else {
                        var message = "Error de ejecución: " + xhr.status + " " + xhr.statusText;
                        swal(
                            'Error!!',
                            message,
                            'error'
                        )
                    }

                },
                // código a ejecutar sin importar si la petición falló o no
                complete: function (xhr, status) {
                    // fincarga();
                }
            });
        }


    </script>


@stop

