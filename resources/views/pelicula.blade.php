@extends('layouts.layout   ')

@section('styles')



@stop

@section('content')

    <section class="">
        <div class="container">
            <div class="row align-items-center">
                <div class="schedule-table-item">
                    <div class="row align-items-center">
                        <div class="col-lg-3"><a href="#" class="schedule-item-image"><img id="imgPortada" src="img/android.svg" alt="..." class="img-fluid"></a></div>
                        <div class="col-lg-9">
                            <h3 class="schedule-item-name" id="titulo"></h3>
                            <span class="schedule-item-genre gradient-1">Science fiction</span>
                            <p class="schedule-item-description" id="descripcion">
                            <div class="schedule-item-time">
                                <div class="viewing-times">
                                    <span><i class="icon ion-md-time"></i>(<span id="fecha"></span>)</span>
                                </div>
                                <div class="duration">Alquilada<span class="certification">15</span> veces</div>
                            </div>

                            <div class="col text-center consesion" id="divReserva">

                            </div>
                        </div>
                    </div>
            </div>
        </div>
        </div>
    </section>


@stop

@section('scripts')

    <script>

        
        $(function () {
            var pelicula = sessionStorage.getItem('pelicula');

            $.ajax({
                url: "{{env("APP_URL_SERVER_API")}}pelicula/show/"+pelicula,
                data: {},
                type: 'GET',
                dataType: 'json',
                beforeSend: function () {
                    //cargando();
                },
                success: function (result) {

                    $("#imgPortada").attr("src","{{env("APP_URL_SERVER")}}imagenes/"+result.data.portada);
                    $("#titulo").html(result.data.nombre);
                    $("#descripcion").html(result.data.descripcion);
                    $("#fecha").html(result.data.fecha_lanzamiento);
                    if(localStorage.getItem("username")){

                        $("#divReserva").html("<button class='btn btn-primary' onclick='reservar("+result.data.id+")'> RESERVAR</button>");
                    }


                },
                error: function (xhr, status) {
                    if (xhr.status == 401) {

                        $("#errorLogin").html(xhr.responseJSON.error);
                        $("#alertErrorLogin").show();

                    } else {
                        var message = "Error de ejecución: " + xhr.status + " " + xhr.statusText;
                        swal(
                            'Error!!',
                            message,
                            'error'
                        )
                    }

                },
                // código a ejecutar sin importar si la petición falló o no
                complete: function (xhr, status) {
                    // fincarga();
                }
            });


        });

    </script>


@stop

