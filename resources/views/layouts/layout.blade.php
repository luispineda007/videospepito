<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Showtracker landing page</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link href="{{asset('plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Ionicons CSS-->
    <link href="{{asset('css/ionicons.min.css')}}" rel="stylesheet">
    <!-- Device mockups CSS-->
    <link href="{{asset('css/device-mockups.css')}}" rel="stylesheet">
    <!-- Google fonts - Source Sans Pro-->
    <!--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700"> -->
    <!-- Swiper sLider-->
    <link href="{{asset('plugins/swiper/css/swiper.min.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/sweetalert/sweetalert.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/jquery/jquery.gritter.css')}}" rel="stylesheet">
    <!-- theme stylesheet-->
    <link href="{{asset('css/style.default.css')}}" rel="stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/favicon.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

    @yield('styles')

</head>
<body>


@include('layouts.menutop')


<div class="page-holder">

    @yield('content')

</div>

@include('layouts.footer')

<!-- JavaScript files-->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('plugins/popper.js/umd/popper.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('plugins/jquery.cookie/jquery.cookie.js')}}"></script>
<script src="{{asset('plugins/swiper/js/swiper.min.js')}}"></script>
<script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('plugins/jquery/jquery.gritter.min.js')}}"></script>
<script src="{{asset('js/aes.js')}}"></script>
<script src="{{asset('js/front.js')}}"></script>

<script>

    $(function () {

        //validamos que el tiempo de vida del token este vijente, si ya termino borramos el LocalStorge
        if (localStorage.getItem("time_token") - new Date().getTime() < 0) {
            localStorage.clear();
            $("#nav-user-name").text("");
            $("#nav-user").hide();
            $("#nav-login").show();
        }

        //Validamos si existe un usuario loggeado y si no mosmtramos los botones para iniciar sesion
        if (localStorage.getItem("username")) {
            $("#nav-user-name").text(AESdencrypt(localStorage.getItem("username"),localStorage.getItem("access_token")));
            $("#nav-user").show();
            $("#itemMisReservas").show();
            if (localStorage.getItem("rol")) {
                $("#itemAlquilar").show();
                $("#itemUsuarios").show();
            }
            $("#nav-login").hide();
        } else {
            $("#nav-user-name").text("");
            $("#nav-user").hide();
            $("#itemMisReservas").hide();
            $("#nav-login").show();
        }

        $("#formLogin").submit(function (e) {
            e.preventDefault();
            var formLogin = $(this);
            $.ajax({
                url: "{{env("APP_URL_SERVER_API")}}auth/login",
                data: formLogin.serialize(),
                type: 'POST',
                dataType: 'json',
                beforeSend: function () {
                    //cargando();
                },
                success: function (result) {

                    //almacenamos en el LocalStorage los dados importantes de la sesion
                    localStorage.setItem('access_token', result.access_token);
                    localStorage.setItem('time_token', new Date().getTime() + (result.expires_in * 1000));
                    localStorage.setItem('username', result.user.nickname);


                    if (AESdencrypt(result.user.rol,result.access_token) == "Admin") {
                        localStorage.setItem('rol', result.user.rol);
                    }

                    $("#nav-user-name").text(AESdencrypt(result.user.nickname,result.access_token));
                    $("#login").modal("hide");
                    $("#nav-user").show();
                    $("#itemMisReservas").show();
                    $(".consesion").show();
                    if (localStorage.getItem("rol")) {
                        $("#itemAlquilar").show();
                        $("#itemUsuarios").show();
                    }
                    $("#nav-login").hide();


                },
                error: function (xhr, status) {
                    if (xhr.status == 401) {

                        $("#errorLogin").html(xhr.responseJSON.message);
                        $("#alertErrorLogin").show();

                    } else {
                        var message = "Error de ejecución: " + xhr.status + " " + xhr.statusText;
                        swal(
                            'Error!!',
                            message,
                            'error'
                        )
                    }

                },
                // código a ejecutar sin importar si la petición falló o no
                complete: function (xhr, status) {
                    // fincarga();
                }
            });
        });

        $("#formRegister").submit(function (e) {
            e.preventDefault();
            var formLogin = $(this);
            $.ajax({
                url: "{{env("APP_URL_SERVER_API")}}user/store",
                data: formLogin.serialize(),
                type: 'POST',
                dataType: 'json',
                beforeSend: function () {
                    //cargando();
                },
                success: function (result) {

                },
                error: function (xhr, status) {
                    if (xhr.status == 422) {

                        data = xhr.responseJSON.data;

                        var error = "<ul>";

                        for (var key in data) {

                            error += "<li>"+ data[key] + "</li>";
                        }

                        error += "</ul>";
                        console.log(error);

                        $("#errorRegister").html(error);
                        $("#alertErrorRegister").show();

                    } else {
                        var message = "Error de ejecución: " + xhr.status + " " + xhr.statusText;
                        swal(
                            'Error!!',
                            message,
                            'error'
                        )
                    }

                },
                // código a ejecutar sin importar si la petición falló o no
                complete: function (xhr, status) {
                    // fincarga();
                }
            });
        });
    });

    function logout() {
        $.ajax({
            url: "{{env("APP_URL_SERVER_API")}}auth/logout",
            data: {"token": localStorage.getItem('access_token')},
            type: 'POST',
            dataType: 'json',

            success: function (result) {

                localStorage.clear();
                $("#nav-user-name").text("");
                $("#nav-user").hide();
                $("#itemMisReservas").hide();
                $("#itemAlquilar").hide();
                $("#itemUsuarios").hide();
                $(".consesion").hide();
                $("#nav-login").show();
                window.location = "/";

            },
            error: function (xhr, status) {
                if (xhr.status == 401) {

                    $("#errorLogin").html(xhr.responseJSON.error);
                    $("#alertErrorLogin").show();

                } else {
                    var message = "Error de ejecución: " + xhr.status + " " + xhr.statusText;
                    swal(
                        'Error!!',
                        message,
                        'error'
                    )
                }

            },
            // código a ejecutar sin importar si la petición falló o no
            complete: function (xhr, status) {
                // fincarga();
            }
        });
    }

    function pelicula(id) {
        sessionStorage.setItem('pelicula', id);
        window.location = "pelicula";
    }

    function reservar(id) {
        $.ajax({
            url: "{{env("APP_URL_SERVER_API")}}reservar",
            data: {"token": localStorage.getItem('access_token'), "pelicula_id": id},
            type: 'POST',
            dataType: 'json',

            success: function (result) {
                jQuery.gritter.add({
                    title: '¡Bien!',
                    text: result.message,
                    class_name: 'growl-success',
                    image: '{{asset("img/plugins/screen.png")}}',
                    sticky: false,
                    time: ''
                });

            },
            error: function (xhr, status) {
                if (xhr.status == 401) {

                    $("#errorLogin").html(xhr.responseJSON.error);
                    $("#alertErrorLogin").show();

                } else {
                    var message = "Error de ejecución: " + xhr.status + " " + xhr.statusText;
                    swal(
                        'Error!!',
                        xhr.responseJSON.message,
                        'error'
                    )
                }

            },
            // código a ejecutar sin importar si la petición falló o no
            complete: function (xhr, status) {
                // fincarga();
            }
        });
    }

</script>


@yield('scripts')

</body>
</html>
