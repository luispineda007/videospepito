<!-- navbar-->
<header class="header">
    <nav class="navbar navbar-expand-lg">
        <div class="container">
            <!-- Navbar brand--><a href="{{route("home")}}" class="navbar-brand font-weight-bold">Videos Pepito</a>
            <!-- Navbar toggler button-->
            <button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right">Menu<i class="icon ion-md-list ml-2"></i></button>
            <div id="navbarSupportedContent" class="collapse navbar-collapse">
                <ul class="navbar-nav mx-auto ml-auto">
                <?php
                function activeMenu( $url ) {
                  return request()->is( $url ) ? 'active' : '';
                }
                ?>
                    <!-- Link-->
                    <li class="nav-item">
                        <a href="{{route("catalogo")}}" class="nav-link {{activeMenu('catalogo')}}">Catálogo</a>
                    </li>
                    <!-- Link-->
                    <li class="nav-item" id="itemMisReservas" style="display: none">
                        <a href="{{route("misreservas")}}" class="nav-link {{activeMenu('misreservas')}}">Mis Reservas</a>
                    </li>
                    <!-- Link-->
                    <li class="nav-item" id="itemAlquilar" style="display: none">
                        <a href="{{route("alquilar")}}#" class="nav-link {{activeMenu('alquilar')}}">Alquilar</a>
                    </li>
                    <!-- Link-->
                    <li class="nav-item" id="itemUsuarios" style="display: none">
                        <a href="{{route("usuarios")}}#" class="nav-link {{activeMenu('usuarios')}}">Usuarios</a>
                    </li>
                    <!-- Link-->
                    <li class="nav-item">
                        <a href="#" class="nav-link">Contactenos</a>
                    </li>
                </ul>
                <ul id="nav-login" class="navbar-nav" style="display: none">
                    <li class="nav-item"><a href="#" data-toggle="modal" data-target="#login" class="nav-link font-weight-bold mr-3">Iniciar Sesión</a></li>
                    <li class="nav-item"><a href="#" data-toggle="modal" data-target="#modalRegister" class="navbar-btn btn btn-primary">Registrate</a></li>
                </ul>
                <ul id="nav-user" class="navbar-nav" style="display: none">
                    <li class="nav-item dropdown"><a id="pages" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><span id="nav-user-name"></span></a>
                        <div class="dropdown-menu">
                            <a href="index.html" class="dropdown-item">Peril</a>
                            <a href="#" class="dropdown-item" onclick="logout()">
                                Cerrar Sesión
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<!-- Login Modal-->
<div id="login" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade bd-example-modal-lg">
    <div role="document" class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header border-bottom-0">
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body p-4 p-lg-5">
                <form action="#" class="login-form text-left" id="formLogin">
                    <div class="form-group mb-4">
                        <label>Correo electrónico</label>
                        <input type="email" name="email" placeholder="name@company.com" class="form-control" required>
                    </div>
                    <div class="form-group mb-4">
                        <label>Contraseña</label>
                        <input type="password" name="password" placeholder="contraseña" class="form-control" required>
                    </div>

                    <div id="alertErrorLogin" class="alert alert-danger alert-dismissible" style="display: none">
                        <strong>Error!</strong> <span id="errorLogin"></span>
                    </div>

                    <div class="form-group">
                        <input type="submit" value="Iniciar Sesión" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Register Modal-->
<div id="modalRegister" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade bd-example-modal-lg">
    <div role="document" class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header border-bottom-0">
                <h5>Registrate</h5>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body p-4 p-lg-5">
                <form action="#" class="text-left" id="formRegister">
                    <div class="row">
                        <div class="form-group mb-4 col-md-6">
                            <label>Documento</label>
                            <input type="text" name="documento" placeholder="112046353" class="form-control" onkeypress="return soloNumeros(event);" required>
                        </div>
                        <div class="form-group mb-4 col-md-6">
                            <label>Nickname</label>
                            <input type="text" name="nickname" placeholder="luis.pineda" class="form-control" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group mb-4 col-md-6">
                            <label>Nombre</label>
                            <input type="text" name="nombre" placeholder="Luis Carlos Pineda" class="form-control" required>
                        </div>
                        <div class="form-group mb-4 col-md-6">
                            <label>Telefono</label>
                            <input type="text" name="telefono" placeholder="3138585565" class="form-control" onkeypress="return soloNumeros(event);" required>
                        </div>
                    </div>
                    <div class="form-group mb-4">
                        <label>Correo electrónico</label>
                        <input type="email" name="email" placeholder="luis.pineda@ejemplo.com" class="form-control" required>
                    </div>

                    <div class="row">
                        <div class="form-group mb-4 col-md-6">
                            <label>Contraseña</label>
                            <input type="password" name="password" placeholder="contraseña" class="form-control" required>
                        </div>
                        <div class="form-group mb-4 col-md-6">
                            <label>Confirmar Contraseña</label>
                            <input type="password" name="password_confirmation" placeholder="confirmar contraseña" class="form-control">
                        </div>
                    </div>




                    <div id="alertErrorRegister" class="alert alert-danger alert-dismissible" style="display: none">
                        <strong>Error!</strong> <span id="errorRegister"></span>
                    </div>

                    <div class="form-group">
                        <input type="submit" value="Iniciar Sesión" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>