@extends('layouts.layout   ')

@section('styles')



@stop

@section('content')

    <section class="">
        <div class="container">
            <div class="row align-items-center">

            </div>
        </div>
    </section>


    <!-- Hero Section-->
    <section class="hero shape-1">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <h1 class="hero-heading">Gracias por seguir con nosotros</h1>
                    <p class="lead mt-5 font-weight-light"> Videos Pepitp tomó la decisión de migrar
                        sus servicios a la web, de manera que ustedes nuestros clientes tendran disponible desde cualquier lugar
                        la información de los títulos ofrecidos y operaciones básicas de consulta y reserva. 🍿</p>
                </div>
                <div class="col-lg-6 d-none d-lg-block">
                    <div class="device-wrapper mx-auto">
                        <div data-device="iPhone7" data-orientation="portrait" data-color="black" class="device">
                            <div class="screen"><img src="img/screen.jpg" alt="..." class="img-fluid"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Features Section-->
    <section class="features shape-2">

        <div class="container">
            <h2> Peliculas en Cartelera</h2>
        </div>

        <div class="swiper-container tv-shows-slider">
            <div id="encartelera" class="swiper-wrapper">


            </div>

            <!-- If we need navigation buttons -->
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>

        </div>
    </section>

    <!-- Testimonials Section-->
    <section class="testimonials bg-black mt-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 section-padding">
                    <div class="section-header pr-3"><span class="section-header-title text-white">Comunidad</span>
                        <h2 class="h1 text-white">Ustedes nuestros clientes no hacer ser mejores cada día</h2>
                        <p class="lead text-white mt-4 mb-4">Todos sus comentarios hacen crecer a nuestra tienda Muchas Gracias por seguir con nosotros</p>
                    </div>
                </div>
                <div class="col-lg-6 d-none d-lg-block">
                    <div class="row feeds">
                        <div class="col-lg-6" style="padding-top: 200px">
                            <div class="swiper-container testimonials-slider-1">
                                <div class="swiper-wrapper">
                                    <!-- Feed slide-->
                                    <div class="swiper-slide">
                                        <!-- Feed block-->
                                        <div class="feed-block">
                                            <div class="feed-header">
                                                <div class="feed-user">
                                                    <div class="feed-user-avatar"><img src="img/testimonial-avatar-1.svg" alt="user" class="feed-user-image img-fluid"></div>
                                                    <div class="feed-user-name"><strong>Patrick Martin</strong></div>
                                                </div>
                                                <div class="feed-icon"><i class="icon ion-logo-twitter"></i></div>
                                            </div>
                                            <div class="feed-body">
                                                <p class="feed-text">Videos pepito el mejor lugar para alquilar peliculas!</p>
                                            </div>
                                            <div class="feed-date">Jun 27, 2019</div>
                                        </div>
                                    </div>
                                    <!-- Feed slide-->
                                    <div class="swiper-slide">
                                        <!-- Feed block-->
                                        <div class="feed-block">
                                            <div class="feed-header">
                                                <div class="feed-user">
                                                    <div class="feed-user-avatar"><img src="img/testimonial-avatar-3.svg" alt="user" class="feed-user-image img-fluid"></div>
                                                    <div class="feed-user-name"><strong>Joseph Matthews</strong></div>
                                                </div>
                                                <div class="feed-icon"><i class="icon ion-logo-twitter"></i></div>
                                            </div>
                                            <div class="feed-body">
                                                <p class="feed-text">Videos Pepito hace una tarde de peliculas espectacular </p>
                                            </div>
                                            <div class="feed-date">Jun 26, 2019</div>
                                        </div>
                                    </div>
                                    <!-- Feed slide-->
                                    <div class="swiper-slide">
                                        <!-- Feed block-->
                                        <div class="feed-block">
                                            <div class="feed-header">
                                                <div class="feed-user">
                                                    <div class="feed-user-avatar"><img src="img/testimonial-avatar-5.svg" alt="user" class="feed-user-image img-fluid"></div>
                                                    <div class="feed-user-name"><strong>Bruce Murphy</strong></div>
                                                </div>
                                                <div class="feed-icon"><i class="icon ion-logo-twitter"></i></div>
                                            </div>
                                            <div class="feed-body">
                                                <p class="feed-text">La varidead y cantidad de peliculas que podemos encontrar en Videos Pepitpo lo hace el lugar perfecto para alquilar peliculas!</p>
                                            </div>
                                            <div class="feed-date">Jun 26, 2019</div>
                                        </div>
                                    </div>
                                    <!-- Feed slide-->
                                    <div class="swiper-slide">
                                        <!-- Feed block-->
                                        <div class="feed-block">
                                            <div class="feed-header">
                                                <div class="feed-user">
                                                    <div class="feed-user-avatar"><img src="img/testimonial-avatar-3.svg" alt="user" class="feed-user-image img-fluid"></div>
                                                    <div class="feed-user-name"><strong>Crystal Perkins</strong></div>
                                                </div>
                                                <div class="feed-icon"><i class="icon ion-logo-twitter"></i></div>
                                            </div>
                                            <div class="feed-body">
                                                <p class="feed-text">El sistema de reservar las peliculas desde cualquier lugar hace que videos pepito sea el mejor </p>
                                            </div>
                                            <div class="feed-date">Jun 24, 2019</div>
                                        </div>
                                    </div>
                                    <!-- Feed slide-->
                                    <div class="swiper-slide">
                                        <!-- Feed block-->
                                        <div class="feed-block">
                                            <div class="feed-header">
                                                <div class="feed-user">
                                                    <div class="feed-user-avatar"><img src="img/testimonial-avatar-1.svg" alt="user" class="feed-user-image img-fluid"></div>
                                                    <div class="feed-user-name"><strong>Patrick Martin</strong></div>
                                                </div>
                                                <div class="feed-icon"><i class="icon ion-logo-twitter"></i></div>
                                            </div>
                                            <div class="feed-body">
                                                <p class="feed-text">Videos Pepito el Mejor!</p>
                                            </div>
                                            <div class="feed-date">Jan 23, 2019</div>
                                        </div>
                                    </div>
                                    <!-- Feed slide-->
                                    <div class="swiper-slide">
                                        <!-- Feed block-->
                                        <div class="feed-block">
                                            <div class="feed-header">
                                                <div class="feed-user">
                                                    <div class="feed-user-avatar"><img src="img/testimonial-avatar-4.svg" alt="user" class="feed-user-image img-fluid"></div>
                                                    <div class="feed-user-name"><strong>Bruce Murphy</strong></div>
                                                </div>
                                                <div class="feed-icon"><i class="icon ion-logo-twitter"></i></div>
                                            </div>
                                            <div class="feed-body">
                                                <p class="feed-text">Me gustaria que despues de realizar la reserva me llevaran la pelicula a mi casa :) </p>
                                            </div>
                                            <div class="feed-date">Jan 19, 2019</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@stop

@section('scripts')

    <script>

        var muSwiper;

        $(function () {

            $.ajax({
                url: "{{env("APP_URL_SERVER_API")}}pelicula/peliculasfiltro",
                data: {"cartelera": "X", "perPage": 15},
                type: 'GET',
                dataType: 'json',
                beforeSend: function () {
                    //cargando();
                },
                success: function (result) {

                    $.each(result.data.data, function (i, d) {

                        var html = "<div class='swiper-slide'><a href='#' onClick='pelicula("+d.id+")' class='tv-shows-link'><img src='{{env("APP_URL_SERVER")}}imagenes/" + d.portada + "' alt='...' class='tv-shows-image img-fluid'></a></div>";
                        $("#encartelera").append(html);

                    });
                    muSwiper = new Swiper(".swiper-container", {
                        width: 280,
                        loop: true,
                        pagination: {
                            el: '.swiper-pagination',
                            type: 'bullets',
                        },
                        navigation: {
                            nextEl: '.swiper-button-next',
                            prevEl: '.swiper-button-prev',
                        },

                    });



                },
                error: function (xhr, status) {
                    if (xhr.status == 401) {

                        $("#errorLogin").html(xhr.responseJSON.error);
                        $("#alertErrorLogin").show();

                    } else {
                        var message = "Error de ejecución: " + xhr.status + " " + xhr.statusText;
                        swal(
                            'Error!!',
                            message,
                            'error'
                        )
                    }

                },
                // código a ejecutar sin importar si la petición falló o no
                complete: function (xhr, status) {
                    // fincarga();
                }
            });

        });

    </script>


@stop
